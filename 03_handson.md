# Git hands on (3rd lecture 2024-03-04)

Prerequisities:

```bash
# Install GIT on RedHat/CentOS/Rocky
yum install git

# Install GIT on Ubuntu/Debian
apt install git

# Export environment variables in ~/.bashrc
export GIT_AUTHOR_NAME="Name Surname"
export GIT_AUTHOR_EMAIL=name@domain.tld
export GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"
export GIT_COMMITTER_EMAIL="$GIT_AUTHOR_EMAIL"

```

1. Clone the repository for Ruby on Rails framework at https://github.com/rails/rails
   1. Explore the version history by visualizing it as a graph.
   1. Who was the last person to modify README.md? (Hint: use git log with an argument)
   1. What is the oldest commit message and year? What tool was used before Git in this project?
   1. What was the commit message associated with the last modification to the Dalli GEM, line of Gemfile? (Hint: use git blame and git show)
   1. Modify README.md. What happens when you do `git stash`? Run `git stash list`. Inspect the repository. Run `git stash pop` to undo what you did with git stash. In what scenario might this be useful?
   1. Git provides a configuration file (or dotfile) called `~/.gitconfig`. Create an alias in `~/.gitconfig` so that when you run `git graph`, you get the output of `git log --all --graph --decorate --oneline`. Information about git aliases can be found [here](https://git-scm.com/docs/git-config#Documentation/git-config.txt-alias).
   1. BASH provides a configuration file called `~/.bashrc`. Create aliases in `~/.bashrc` so that when you run `ggl`, you get the output of `git log --all --graph --decorate --oneline` and when you run `ggh`, you get the output of `git graph`.
1. `git reset` comparison
   1. Within Rails local Git repository try and inspect:
      1. Inspect log with `git log`, find a log older than HEAD, e.g. 4-5 commits back, grab commit id
      1. `git reset <commit id> --soft`
      1. Inspect status with `git status`
      1. Clean up repo with `git restore --staged <file>` `git checkout <file>`
      1. Repull from upstream `git pull --rebase`
   1. Continue within the repo
      1. Inspect log with `git log`, find a log older than HEAD, e.g. 4-5 commits back, grab commit id
      1. `git reset <commit id>`
      1. Inspect status with `git status`
      1. Clean up repo with `git checkout <file>`
      1. Repull from upstream `git pull --rebase`
   1. Continue within the repo
      1. Inspect log with `git log`, find a log older than HEAD, e.g. 4-5 commits back, grab commit id
      1. `git reset <commit id> --hard`
      1. Inspect status with `git status`
1. This is hook hands on:
   1. Create new folder `03_hooks`.
   1. Add to foled files `client.sh` and `server.sh` from the lecture repository.
   1. Init Git repository, add and commit both files.
   1. Copy commit script `pre-commit` to `.git/hooks` folder.
   1. Inspect `pre-commit` script.
   1. Add a new line with variable VERSION to both `client.sh` and `server.sh` script.
   1. Inspect repo with `git status` and commit changes.
   1. Inspect both Bash files. Are there any changes?
1. Moving hooks into repository hands on:
   1. Configure git to use inrepo hook foldder with `git config --local core.hooksPath .githooks/`.
   1. Create `.githooks` folder in the repo.
   1. Move `pre-commit` hook script from `.git/hooks` into `.githooks`.
   1. Inspect repo with `git status` and commit changes.
   1. Modify one of Bash files, inspect repo and commit changes.
   1. Inspect the  Bash file. Are there any changes?
