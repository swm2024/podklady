# Gitlab hands on tips (2nd lecture 2024-02-26)
1.
   ```console
   mkdir /tmp/missing
   ```
1.
   ```console
   man touch
   ```
1.
   ```console
   touch /tmp/missing/semester
   ```
1. None
1. It prints `-bash: ./semester: Permission denied` Type `ls -l` to look at the permission bits of the file. The first part of the print should look like `-rw-r--r--`, which means execution is not permitted but only reading (as denoted in _r_) and writing (as denoted in _w_).
1. Type `man sh`. It prints `sh` is a POSIX-compliant command interpreter. `sh` specifies to the shell that the file `semester` is supposed to be interpreted i.e. executed using `sh`.
1. Type `man chmod`. It prints `chmod` changes file modes or Access Control Lists. Adding the tag `+x` before the file name argument would change the file mode to be executable.
1.
   ```console
   chmod +x ./semester
   ./semester
   ```
1.
   ```console
   date -r ./semester > ~/last-modified.txt
   ```
1.
   ```console
   ./semester | grep last-modified > ~/web-last-modified.txt
   ```

---

1.
   Linux:

   ```console
   ls -lath --color=auto
   ```

   macOS:

   ```console
   ls -lathG
   ```
1. `marco.sh`:

   ```bash
   #!/usr/bin/env bash

   marco() {
       export MARCO=$(pwd)
   }

   polo() {
       cd "$MARCO"
   }
   ```

   Begin `marco.sh` with the shebang line for Bash because `marco` and `polo` are defined to be Bash functions. `export` command ensures the value `MARCO` is set to be an environment variable and not a local variable confined within the scopes of the `marco()` function only. This way `polo()` can read the value of `MARCO` defined outside its scope. `source` command is used to execute the file since `./marco.sh` will not be executed. (Permission Denied)
1. `debug.sh`:

   ```bash
   #!/usr/bin/env bash

   count=0
   until [[ "$?" -ne 0 ]];
   do
     count=$((count+1))
     ./random.sh &> out.txt
   done

   echo "found error after $count runs"
   cat out.txt
   ```
1. Linux:

   ```
   find . -type f -name "*.html" | xargs -d '\n'  tar -cvzf archive.tar.gz
   ```

   (`-d '\n'` uses a newline as delimiter to split file names)

   macOS:
   ```
   find . -type f -name "*.html" -print0 | xargs -0  tar -cvzf archive.tar.gz
   ```

   (`-print0` uses a null character to split file names, and `-0` uses it as delimiter)
1.
   ```console
   find . -type f | xargs ls -t | head -n 1
   ```

   - Find all files in the current directory recursively.
   - List all files by recency.
   - Find the most recently modified file in a directory i.e. the first line of the list.


