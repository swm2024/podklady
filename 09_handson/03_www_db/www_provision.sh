#!/bin/bash
#provision WebBox Version 0.0.0
# Edited by Jessica Rankins 4/5/2017

rm -f postinstall.sh

#install apache and php
apt-get update
#apt-get install -y apache2 php5 libapache2-mod-php5 php5-mcrypt php5-mysql mysql-client bash-completion


DBPASSWD=rootpass
debconf-set-selections <<< 'mysql-server mysql-server/root_password password rootpass'^M
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password rootpass'^M

#debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
#debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $DBPASSWD"
#debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $DBPASSWD"
#debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $DBPASSWD"
#debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none"
#debconf-set-selections <<< "phpmyadmin phpmyadmin/remote/newhost string 192.168.56.6"
#debconf-set-selections <<< "phpmyadmin phpmyadmin/remote/host select 192.168.56.6"
#export DEBCONF_FRONTEND=noninteractive
#apt-get install -yq --force-yes phpmyadmin

apt install -y apache2 php bash-completion mysql-client adminer
systemctl start apache2

a2enconf adminer
systemctl restart apache2

echo cd / >> /home/vagrant/.bashrc
echo "Hello World from WWW!"
