# Gitlab hands on (4th lecture 2024-03-11)

## Part I

1. Create an empty project in your Gitlab namespace with README.md. (Hint: Make sure it's a public one and initialised with a README.)
   1. Clone your new repository to your computer `/tmp/_give_name_`.
   1. Are there any commits in the repository yet?
   1. What are the 'remotes' of your Git repository?
   1. Modify README.md, commit, and push.
   1. What has happend on Gitlab project view?
1. Create empty project in your Github namespace. (Hint: Public one, no content at all.)
   1. Go to your local copy of Gitlab project `/tmp/_give_name_` and create a new remote called `gh`. (Hint:`git remote ...`, might be described a new Github repositry...)
   1. Try to push a copy to `gh` remote

## Part II

Before proceeding further make sure to create a group of two before.

1. Create an empty project in your Gitlab namespace with README.md. (Hint: Make sure it's a public one and initialised with a README.)
   1. Create basic ticket labels within your project.
   1. Grant project access rights to your teammate. (Hint: maintainer role)
   1. Create a new ticket within the project with a task to modify README.md.
      1. Try a few Markdown features within the text. (Optional)
      1. Try adding a reference to your teammate with @.
      1. Try adding a label to ticket.
   1. Grant project access rights to your teammate.
   1. Assign the ticket to your teammate.
1. Watch your notifications within Gitlab.
   1. Find your assigned ticket(s)
1. Clone your teammate new repository to your computer `/tmp/_give_name_`.
   1. Make a comment in the ticket.
      1. Try adding another label.
   1. Create a new branch.
   1. Modify README.md.
   1. Commit the changes (Hint: Do not forget to close the ticket with commit message.)
   1. Push the repo upstream.
   1. Create a merge request by clicking on a MR link.
      1. Assign MR to your teammate.
      1. Write a comment within MR.
      1. Try adding a label to it.
1. Watch your notifications within Gitlab.
   1. Find your assigned MR(s).
   1. Add commnet within the MR.
   1. Try merging the MR. (Hint: Force branch deleting.)
