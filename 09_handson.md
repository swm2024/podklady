# Gitlab hands on (9th lecture)

## Prerequisities Windows (Native)

1. Vagrant - https://www.vagrantup.com/downloads
1. Install VirtualBox - https://www.virtualbox.org/wiki/Downloads

Hint: https://www.vagrantup.com/docs/installation#windows-virtualbox-and-hyper-v

## Prerequisities Windows (WSL)

1. Uninstall Desktop Docker
1. Make sure your WSL username is the same as Win username - you might need `ubuntu config --default-user new_user_name`
   1. Switch WSL to root
   1. Start WSL
   1. Create new user `adduser username`
   1. Add user to sudo group `vim /etc/groups`
   1. Exit
   1. Switch WSL to username
   1. Start WSL
1. Install VirtualBox - https://www.virtualbox.org/wiki/Downloads
1. Install PowerShell 7.x - https://docs.microsoft.com/cs-cz/powershell/scripting/install/installing-powershell-on-windows?view=powershell-7.2#msi
1. Within WSL install:
   1. Vagrant - https://www.vagrantup.com/downloads
   1. Use env variables (make sure you have proper paths - see https://thedatabaseme.de/2022/02/20/vagrant-up-running-vagrant-under-wsl2/
   ```bash
   export VAGRANT_WSL_ENABLE_WINDOWS_ACCESS="1"
   export PATH="$PATH:/mnt/c/Program Files/Oracle/VirtualBox"
   export PATH="$PATH:/mnt/c/Windows/System32"
   export PATH="$PATH:/mnt/c/Program\ Files/PowerShell/7"
   ```
   1. Install plugin `vagrant plugin install virtualbox_WSL2`
   1. Modify `/etc/wsl.conf` to have:
   ```ini
   # Enable extra metadata options by default
   [automount]
   enabled = true
   root = /mnt/
   options = "metadata,umask=77,fmask=11"
   mountFsTab = false
   ```
1. Run PowerShell with admin rights:
```powershell
Restart-Service -Name "LxssManager"
```
1. Make sure you run `Vagrantfile` from Windows mountpoint - e.g. `/mnt/c/Users/username/Documents/vagrant-test/`

## Prerequisities Linux

1. Vagrant - https://www.vagrantup.com/downloads
1. Install VirtualBox - https://www.virtualbox.org/wiki/Downloads

## Task 0 - Vagrant up

1. Verify Vagrant installation - `vagrant init hashicorp/bionic64`
1. Open VirtualBox
1. Up you go! - `vagrant up`
1. Try to SSH -`vagrant ssh`
1. Get status with - `vagrant status`
1. Destroy VM - `vagrant destroy`
1. Inspect your `Vagrantfile`

## Task 1 - Simple

1. Get 01_simple folder
1. Try to up the VM
1. Destroy the VM
1. Modify VM to install Apache and forward the 80 port.
1. Run up VM.
1. Investigate http://127.0.0.1:8080
1. Destroy VM

## Task 2 - Simple

1. Get 02_ansible folder
1. Try to up the VM
1. Destroy the VM
1. If you need to install Ansible - `sudo apt install ansible`
1. Modify VM to install Apache and with Ansible and forward the 80 port.
1. Run up VM.
1. Investigate http://127.0.0.1:8080
1. Destroy VM

## Task 3 - WWW and DB VMs

Hint: VM is using host-interface. You must run vagrant/powershell with admin privileges when using Windows Vagrant (native) scenario.

1. Get 03_www_db folder
1. Inspect the Vagrant
1. Try to up VM
1. Find ip of WWW VM
1. Find ip of DB VM
1. Open browser with WWW ip
   1. Can you see indec page?
   1. Can you open URL http://IP/adminer
1. Try to log in to Adminer with credentials provided n Vagrant file

## Task 4 - Multiple VMs

Hint: VM is using host-interface. You must run vagrant/powershell with admin privileges when using Windows Vagrant (native) scenario.

1. Get folder 04_multiple
1. Run up all the VMs
1. Try to SSH to them
1. Destroy all of them
1. Modify settings so they have IPs 192.168.56.1x
1. Run up all the VMs

## Task 5 - Tmux (optional)

1. Install tmux
1. Try to open multiple panes - one per VM
1. Try command `sync-pasen on/off`

## Task 6 - Clustershell (optional)

1. Install `clustershell`
1. Try parralel SSH with ```clush -bw 192.168.56.[11-13] uptime```
