# Gitlab Page hands on (5th lecture 2024-04-02)

1. Create a new project based on GL pages `Pages/Plain HTML`.
   1. Clone project
   1. Modify `index.html` and push the project.
   1. Inspect CI (continuous integrity) and the content.
   1. Go to project settings - inspect pages setting
   1. Make sure SSL certificate is enabled.
   1. Open the site in a browser.
   1. OPTIONAL - Checked being OK remove the project permanetly.
1. Create a new project based on GL pages Jekyll template.
   1. Go to project CI and hit button `run pipeline`.
   1. Inspect CI (continuous integrity) and the content.
   1. Go to project settings - inspect pages settings.
   1. Make sure SSL certificate is enabled.
   1. Open the site in a browser.
   1. Clone project.
   1. Modify `_config.yml`, correct URL settings and push the project.
   1. Open the site in a browser.
   1. Go to project settings - inspect pages settings.
      1. Try adding custom domain (Hint:You can use own or you will be provided. david@hrbac.cz)
      1. Verificate domain.
      1. Inspect site in a browser.
      1. Modify `_config.yml`, correct URL settings and push the project.
      1. Inspect site in a browser.
   1. Modify page About
      1. Change page tile.
      1. Change page permalink. (Hint: e.g. `permalink: /about-me/`)
      1. Commit, push, and inspect CI.
      1. Go to browser and check link and page title.
   1. Create custom 404 page, see (https://docs.gitlab.com/ee/user/project/pages/introduction.html#custom-error-codes-pages) (Hint: Use own HTML page or "Hello WOrld" page, use Google.)
      1. Get it worked
   1. Create custom redirecr /about/ -> /about-me/ with code 302 (https://docs.gitlab.com/ee/user/project/pages/redirects.html#create-redirects) (Hint: you need to modify config with `include` directive.)
      1. Make a test
   1. Checked being OK remove the project permanetly.
1. Create a new ticket and try adding special GL references. (https://docs.gitlab.com/ee/user/markdown.html#gitlab-specific-references)
   1. issue
   1. issue from different project
   1. commit, MR (also from different project)
   1. user (Hint: Do not use groups, all the members get notification...)
1. Visit https://docs.gitlab.com/ee/user/project/description_templates.html
   1. Read the document.
   1. Try to create a new issue template with task list.
   1. Commit and create a new ticket based on the template.
