# Gitlab hands on (10th lecture)

Handy link: https://stackoverflow.com/questions/16931770/makefile4-missing-separator-stop

## Task 0 - Simple compile makefile

```makefile
blah: blah.o
        cc blah.o -o blah # Runs third

blah.o: blah.c
        cc -c blah.c -o blah.o # Runs second

blah.c:
        echo "#include <stdio.h>\n int main() { printf(\"Hello world!\"); return 0; }" > blah.c # Runs first
```

1. Install make and GCC - `sudo apt install gcc make`
1. Create a Makefile.
1. Type make and space. Hit tab on keyboard. Autocompletion should happen.
1. Try `make blah.c`
1. Inspect local folder. Is there a new file? Inspect the file...
1. Try `make blah`
1. Inspect local folder. Is there a new file? Is the file executable? Can you run it?

## Task 1 - Internal variables

```makefile
files := file1 file2
some_file: $(files)
	echo "Look at this variable: " $(files)
	touch some_file

file1:
	touch file1
file2:
	touch file2

clean:
	rm -f file1 file2 some_file
```

1. Create a Makefile.
1. Type make and space. Hit tab on keyboard. Autocompletion should happen.
1. Inspect all targets and examine folder content.

## Task 2 - Modifying shell

```makefile
cool:
	@echo "Hello from shell: $(SHELL)"
```

1. Make uses `/bin/sh` as a default shell.
1. Try to modify the default shell to BASH or ZSH.
1. Note the `@` character before the line. What does the character do?
